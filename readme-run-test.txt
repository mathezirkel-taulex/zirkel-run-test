Run-Test, Präsenzzirkel Klasse 7/8 2022/23, neu erstellt. Von Alex Mai.

Konzipiert aus einem Zirkel dazu von Ingo von ca 2014.

Zirkel grob in 3 Teile aufgeteilt:
1) erste zwei Experimente
2) herumprobieren und einzelne Details vom Run-Test herausfinden
3) Zettel durcharbeiten und an Tafel erklären


Fazit:

Experiment gut verständlich und alle waren motiviert. Alleine das Münzwerfen ist schon spaßig genug in dem Alter. Bei der Theorie hinter dem Run-Test kamen scheinbar auch alle gut genug mit.



Zum Ablauf:

1) erste zwei Experimente

Damit man nicht 101 Felder abzählen muss, gibt es zuerst das Arbeitsblatt mit den abgezählten Feldern.

Am Anfang sollen die Kinder zuerst ein Experiment mit selbst erfundenen Zahlen machen (im Folgenden: "gefälscht"), danach eins von einer Münze generiert. Nicht anders rum, damit sie nicht direkt sehen, wie eine von einer Münze generierte Zeichenfolge aussieht. In die Felder wird K für Kopf, Z für Zahl geschrieben. Dazu noch sagen, dass die Felder nacheinander ausgefüllt werden sollen, nicht irgendwie durcheinander. Dabei soll aber auf dem Blatt nicht als erstes das gefälschte Experiment eingetragen werden, sondern per Münze entscheiden, wo das gefälschte reingeschrieben wird, sonst ist die erste Durchführung von mir vom Run-Test nicht so spannend.

Hinweise geben zum Münze werfen: muss nicht besonders wild sein, einfach ein paar Drehungen in der Luft und nachdem man eine Seite der gestoppten Münze gesehen hat, nicht noch in der Hand umdrehen oder so.

Als Zirkelleitung drehe ich mich vorne um oder gehe hinten in den Raum, um nicht zu sehen, wo die Kinder ihre Experimente eintragen. Wenn ein Kind die ersten zwei Experimente fertig hat, kann es zu mir kommen und ich bestimme per Run-Test, welches der beiden Experimente gefälscht ist. Danach startet für das Kind Teil 2 des Zirkels.

Wichtig: bei jeder Auswertung eines Experiment eines Kindes, das Ergebnis der Auswertung (bzw die Anzahl Run-Tests) versteckt auf einem Zettel aufschreiben, damit die Anzahl der Wechsel bei Teil 3 besprochen werden kann und um die Ergebnisse den Kindern direkt zu ihren Experimenten bereitzustellen, die sie dann selbst nochmal bestätigen können bei Bedarf.


2) herumprobieren und einzelne Details vom Run-Test herausfinden

Da jedes Kind verschieden lange brauchen wird für Teil 1, startet Teil 2 des Zirkels nicht für jedes Kind gleichzeitig. In Teil 2 sollen die Kinder versuchen, eine Zeichenfolge zu fälschen, die ich mit meinem Run-Test für eine echte Münze halten würde. Ich sage den Kindern dabei, dass mir schon klar ist, dass sie jetzt dafür keine Münze werfen werden -- bzw wenn sie das eine Münze machen lassen, ist es langweilig, da sie ja selbst probieren sollen, den Zufall einer Münze zu imitieren -- ich aber einfach das Ergebnis meines Tests hernehmen werde, um die Zeichenfolge zu bewerten, nicht mein Hintergrundwissen.

Direkt nach Teil 1 und im Laufe von Teil 2 können die Kinder einzeln gefragt werden, welche Strategien sie jetzt versuchen und welche Eigenschaften der echten Münzfolgen sie zu kopieren versuchen. Schließlich wenn einzelne Kinder weit genug gekommen sind, kann der Run-test auch mal "laut gedacht" durchgeführt werden. Dafür mit einem Stift über die Zeichenfolge fahren, während man mit den Augen drübergeht, und laut mitzählen, wie viele Wechsel man zählt. Den Kindern fällt dann eventuell auf, dass man die Anzahl der Wechsel zählt (bzw die Anzahl an Runs, um eins versetzt, wobei die Versetzung in dem Moment nicht auffällt und quasi irrelevant ist). Richtige Vermutungen können bestätigt werden, bzw in die richtige Richtung gehende Vermutungen verbessert werden.

Achtung: Die Anzahl der nicht-Wechsel ist genau gespiegelt zur Anzahl der Wechsel. Sei w die Anzahl der Wechsel, x die Anzahl der nicht-Wechsel. Bei 101 Würfen, was zu 100 möglichen Wechseln führt, gilt dann: x = 100 - w. Was davon gezählt wird, ist deswegen für den Run-Test egal, weil die Graphen für die Wahrscheinlichkeiten der Wechsel oder der nicht-Wechsel aufgrund ihrer Symmetrie um den Erwartungswert 50 herum genau gleich sind.


3) Zettel durcharbeiten und an Tafel erklären
Der Zirkelzettel wurde ausgeteilt und ich habe die Kinder abwechselnd den Zettel vorlesen lassen, wobei ich schwierige Stellen kommentiert habe. Bei Satz 1 zu den Wahrscheinlichkeiten bei 101 Münzwürfen, habe ich an der Tafel am Graphen (siehe auch mittlerer Graph auf Rückseite vom Zettel) dazu veranschaulicht, wie diese Wahrscheinlichkeiten für den Run-Test brauchbar sind. Kapitel 3 vom Zettel, zur praktischen Durchführung, habe ich aus Zeitgründen weggelassen, da wir das Experiment ja erst vorher gemacht hatten.



Mögliche Erweiterungen:

Diese Aufgaben hier habe ich teilweise mündlich beim Erarbeiten des Run-tests gemacht, teilweise am Ende nur an die Tafel geklatscht, als ich darüber geredet habe, dass man auch spaßige Aufgaben zu einfachen Wahrscheinlichkeiten machen kann etc.

- wie viele Wechsel sind möglich bei 101 Würfen? -> 0 bis 100
- Wsk für einen Wechsel? (-> 1/2) Dafür folgende Aufgaben erst lösen:
	- Wsk für K oder Z bei einem Wurf? -> 1/2 (zusammen mit der Info, dass eine Münze sich ihr letztes Ergebnis nicht merken kann, haben wir schon damit die Wsk für einen Wechsel, aber wir wollen es noch bodenständiger machen)
	- Mögliche Ergebnisse bei 2 Würfen? -> KK, ZZ, KZ, ZK -> da sie alle gleich wahrscheinlich sind und 2 von 4 davon einen Wechsel darstellen, ist die Wsk für einen Wechsle genau 2/4 bzw 1/2
- 2 Münzen werfen:
	- Wsk für ein K? -> 1/2
	- Wsk für mindestens ein K? -> 3/4
	- Wsk für höchstens ein K? -> 3/4
	- Wsk für Z, wenn erste Münze K? -> 1/2
	- Wsk für Z, wenn eine Münze K? -> 2/3


