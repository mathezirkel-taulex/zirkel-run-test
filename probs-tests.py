import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import binom
from enum import Enum

options = """
Testing some binomial distribution stuff.
  
  Enter comma-separated integers, arbitrary amount, will be the n evaluated.
"""

# def binomial_probability(k, n, p): # see https://en.wikipedia.org/wiki/Binomial_distribution#Probability_mass_function
#   return scipy.stats.binom(k, n, p)

p = 0.5

class Modes(Enum):
  intervalls = "Intervalls"
  plot = "Plot"


def print_list(li):
  result = ""
  for i in range(len(li)):
    result = f"{result}\n{i}: {li[i]}"
  print(result)

def change_mode(mode):
  if mode == Modes.intervalls:
    mode = Modes.plot
  elif mode == Modes.plot:
    mode = Modes.intervalls
  return mode


def main():

  print(options)

  mode = Modes.intervalls

  intervalls = [0.7, 0.95, 0.997, 0.6827, 0.9545, 0.9973]

  while True:
    selection = input(
      """Choose option:
      h -> help
      q -> exit
      m -> change mode
      enter integers separated by comma -> start doing stuff
      """
    )

    if selection == 'h':
      print(options)
    elif selection == 'q':
      break
    elif selection == 'm':
      print(f"Mode change from {mode},")
      mode = change_mode(mode)
      print(f"to {mode}.\n")
    else:
      try:
        numbers = (int(x) for x in selection.split(","))
        
        for n in numbers:
          print(f"\n  *** n is {n} ***")
          rv = binom(n, p) # this holds all values etc, use rv.pmf(k) to get the probability for k successes, see https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.rv_discrete.html#scipy.stats.rv_discrete or https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.binom.html
          
          print(f"Mode: {mode}")

          if mode == Modes.intervalls:
            for i in intervalls:
              lower = (1 - i) / 2
              print(f"{i*100}%: {rv.ppf(lower)}-{rv.ppf(lower+i)}")
            pass
          elif mode == Modes.plot:
            print("this mode was not implemented properly, as the first mode was enough for its intended use")
            fig, ax = plt.subplots(1, 1)

            # n = 250
            # p = 0.5

            x = np.linspace(0, n, n+1)
            x = np.arange(binom.ppf(0.01, n, p), binom.ppf(0.99, n, p))

            ax.plot(x, binom.pmf(x, n, p), 'bo', ms=8, label='binom pmf')

            ax.vlines(x, 0, binom.pmf(x, n, p), colors='b', lw=5, alpha=0.5)

            plt.show() # see https://stackoverflow.com/questions/36269746/matplotlib-plots-arent-shown-when-running-file-from-bash-terminal
            plt.show(block=True)
          else:
            print("wat")

      except ValueError:
        print("try again")


if __name__ == "__main__":
    main()
