from scipy.stats import binom
import numpy as np
import matplotlib.pyplot as plt

# example taken and adjusted from https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.binom.html

fig, ax = plt.subplots(1, 1)

# n = 9 # Anzahl mögliche Wechsel bei 10 Würfen
# n = 100 # Anzahl mögliche Wechsel bei 101 Würfen
n = 225 # Anzahl mögliche Wechsel bei 226 Würfen
# n = 625 # Anzahl mögliche Wechsel bei 626 Würfen
p = 0.5

# mean, var, skew, kurt = binom.stats(n, p, moments='mvsk')

x = np.linspace(0, n, n+1)
# x = np.arange(binom.ppf(0.01, n, p), binom.ppf(0.99, n, p))

ax.vlines(x, 0, binom.pmf(x, n, p), colors='lightgrey', lw=1, alpha=0.8)

ax.plot(x, binom.pmf(x, n, p), 'b-', ms=8)
ax.plot(x, binom.pmf(x, n, p), 'bo', ms=8)


plt.show() # see https://stackoverflow.com/questions/36269746/matplotlib-plots-arent-shown-when-running-file-from-bash-terminal
plt.show(block=True)